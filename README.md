## Chat Application

### Description

This is a simple chat application written in Java using JavaFX 8. It also includes a server.

### Requirements

- JDK 8u45 or newer
- Eclipse (Optional)

### Setup

- Import the project to Eclipse
- Run `ChatServer.java` to start the server
- Run `ChatApplication.java` to start the client

### General program structure

The program is split into three general parts.

---

**`ChatApplication.java`** - This class holds the `main` method for the client, and will initiate all the JavaFX code. It will also create an instance of the `ChatClient` which will be used to connect to the chat server.

This class holds the JavaFX stage and root-scene. It then changes views via the `setView(BaseView newView)` method to any of the other loaded views. 

There are currently two other views, `ConfigView` which accepts inputs on how to connect to the server, and `ChatView` which contains an input and the conversation-view.

---

**`ChatClient.java`** - This class is responsible for making a connection to the server and exchanging messages.

When a connection is created it creates a new thread that will listen for input from the server and add it to the conversation-view in the `ChatView` class. It also provides the application with a way to send messages using the `sendMessage(String message)` method.

---

**`ChatServer.java`** - This class contains the `main` method for the server and runs separately from the client. It's job is to accept connections from clients and relay their messages to other connected clients.

When a new client connects, it gets its own thread which is responsible for listening to the client and relaying any messages it receives to the other clients currently connected.