package application.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.SocketException;
import java.util.logging.Logger;

/**
 * ClientThread implementation for the Server-side of the application.
 * <p>
 * This class is responsible for receiving input from the client and relaying it
 * to all the other currently connected clients.
 * <p>
 * If a client loses connection, it will stop and remove itself from the
 * <code>clientList</code> list.
 * 
 * @author Sid
 */
public class ClientThread implements Runnable {

    private BufferedWriter outStream;
    private BufferedReader inStream;
    private ChatServer chatServer;

    final private Logger LOG = Logger.getLogger(ClientThread.class.getName());

    /**
     * Constructor.
     * 
     * @param chatServer
     *            Reference back to the ChatServer that owns this thread.
     * @param outStream
     *            Reference to the output stream for this client.
     * @param inStream
     *            Reference to the input stream for this client.
     */
    public ClientThread(ChatServer chatServer, BufferedWriter outStream,
            BufferedReader inStream) {
        this.chatServer = chatServer;
        this.outStream = outStream;
        this.inStream = inStream;
    }

    /**
     * This method will put itself in an endless loop and listen for input from
     * its client.
     * <p>
     * When input is received it relays it back to the ChatServer who in turn
     * sends it out to all other connected clients.
     */
    @Override
    public void run() {
        try {

            // Stay in this infinite loop and relay messages from this
            // client to other clients
            while (true)
                chatServer.sendToClients(inStream.readLine(), this);

        } catch (SocketException e) {

            // If we end up in here, the client has lost the connection
            // And we need to close everything gracefully
            chatServer.removeClient(this);
            LOG.info("Client was lost.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a reference to the BufferedWriter that belongs to this client.
     * 
     * @return a reference to the BufferedWriter associated with this client
     */
    public BufferedWriter getWriter() {
        return this.outStream;
    }
}
