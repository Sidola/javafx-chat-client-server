package application.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * This class acts as the server for all the chat clients.
 * <p>
 * It listens for new clients on port 4242 and creates a new thread for each new
 * client that connects.
 * <p>
 * It will also relay messages from connected clients to all other clients that
 * are currently connected.
 * 
 * @author Sid
 */
public class ChatServer {

    final private int PORT = 4242;
    final private List<ClientThread> clientList = new ArrayList<ClientThread>();

    final private Logger LOG = Logger.getLogger(ChatServer.class.getName());

    /**
     * Starts the server.
     * 
     * @param args
     */
    public static void main(String[] args) {
        ChatServer chatServer = new ChatServer();
        chatServer.start();
    }

    /**
     * This method will open a ServerSocket and then go into an infinite loop
     * while it's waiting for clients to connect.
     * <p>
     * Whenever a client connects, it gives it a new thread and stores a
     * reference to it in the <code>clientList</code> list.
     */
    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            LOG.info("Server has started and is listening for clients.");

            // Go into an infinite loop and listen for connection requests
            while (true) {
                Socket clientSocket = serverSocket.accept();
                LOG.info("A client has connected.");

                // Get streams
                BufferedWriter outStream = new BufferedWriter(new OutputStreamWriter(
                        clientSocket.getOutputStream(), "UTF-8"));
                outStream.flush(); // Flush directly after creating to avoid
                                   // deadlock
                BufferedReader inStream = new BufferedReader(new InputStreamReader(
                        clientSocket.getInputStream(), "UTF-8"));
                LOG.info("Streams to client established.");

                // Create a new thread and store it
                ClientThread clientThread = new ClientThread(this, outStream, inStream);
                clientList.add(clientThread);

                Thread thread = new Thread(clientThread);
                thread.start();
                LOG.info("A new client thread started.");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes a client from the <code>clientList</code>. Usually when the
     * client disconnects from the server.
     * 
     * @param client
     *            Client to be removed.
     */
    protected void removeClient(ClientThread client) {
        clientList.remove(clientList.indexOf(client));
    }

    /**
     * Relays a message from a client to all other connected clients.
     * 
     * @param message
     *            Message to relay.
     * @param sender
     *            The client that sent the message.
     */
    protected void sendToClients(String message, ClientThread sender) {

        LOG.info("Sending message: " + message);

        for (ClientThread clientThread : clientList) {

            // Don't send messages to yourself
            if (clientThread == sender)
                continue;

            try {
                // The trailing line-break is important for the
                // BufferedReader to work correctly
                clientThread.getWriter().write(message + "\n");
                clientThread.getWriter().flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
