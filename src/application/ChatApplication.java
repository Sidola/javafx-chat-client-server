package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import application.client.ChatClient;
import application.model.UserConfig;
import application.view.ChatView;
import application.view.ConfigView;
import application.view.BaseView;

/**
 * Main class for the Chat Application.
 * <p>
 * This class is responsible for creating the JavaFX window and all necessary
 * objects.
 * 
 * @author Sid
 */
public class ChatApplication extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private UserConfig userCfg;

    private ChatClient chatClient;

    private ConfigView configView;
    private ChatView chatView;

    /**
     * Responsible for launching the whole application.
     * 
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Starts the JavaFX application and configures the primary stage.
     * <p>
     * Also gives BaseView a reference back to the main application and
     * instantiates all the other views that will be used.
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Chat Client");
        this.primaryStage.setResizable(false);
        this.primaryStage.setOnCloseRequest(e -> System.exit(0));

        chatClient = new ChatClient(this); // Create a new instance of the chat
                                           // client

        BaseView.setMainApp(this); // Set a reference back to main
        // Init the views after we've set the main reference
        configView = new ConfigView();
        chatView = new ChatView();

        this.initRoot();
    }

    /**
     * Creates the root layout and sets it on the stage.
     * <p>
     * Also sets the ConfigView so the user can connect to the server.
     */
    private void initRoot() {
        rootLayout = new BorderPane();
        Scene scene = new Scene(rootLayout, 400, 300);
        primaryStage.setScene(scene);
        primaryStage.show();

        this.setView(getConfigView());
    }

    /**
     * Appends text to the chat.
     * 
     * @param message
     *            The message to append.
     */
    public void appendToChat(String message) {
        chatView.appendTextToConversation(message);
    }

    /**
     * Starts the chat client that connects to the server.
     * 
     * @throws IOException
     *             Throws an IOException if the server could not be reached.
     */
    public void startChatClient() throws IOException {
        getChatClient().start();
    }

    // ------------------------------
    //
    // Getters & Setters below
    //
    // ------------------------------

    /**
     * Sets a new view in the rootLayout.
     * 
     * @param newView
     *            The view to set in the rootLayout.
     */
    public void setView(BaseView newView) {
        rootLayout.setCenter(newView.getView());
    }

    /**
     * Returns the user config for this session.
     * 
     * @return the current user config
     */
    public UserConfig getUserCfg() {
        return userCfg;
    }

    /**
     * Sets a new user config for this session.
     * 
     * @param userCfg
     *            The user config to use.
     */
    public void setUserCfg(UserConfig userCfg) {
        this.userCfg = userCfg;
    }

    /**
     * Returns a reference to the chat client.
     * 
     * @return reference to the chat client
     */
    public ChatClient getChatClient() {
        return chatClient;
    }

    /**
     * Returns a reference to the chat view.
     * 
     * @return reference to the chat view
     */
    public ChatView getChatView() {
        return chatView;
    }

    /**
     * Returns a reference to the config view.
     * 
     * @return reference to the config view
     */
    public ConfigView getConfigView() {
        return configView;
    }

}