package application.view;

import java.io.IOException;
import java.util.Random;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import application.ChatApplication;
import application.model.UserConfig;

/**
 * This is the config view of the application.
 * <p>
 * Here the user will be able to enter a user name, and the IP and port of the
 * server he wants to connect to.
 * 
 * @author Sid
 */
public class ConfigView extends BaseView {

    private AnchorPane anchorPane = null;
    private VBox vbox;
    private TextField userNameInput;
    private TextField ipInput;
    private TextField portInput;
    private Button startBtn;

    private ChatApplication mainApp = BaseView.getMainApp();

    /**
     * Returns this view to the caller.
     */
    public AnchorPane getView() {
        if (anchorPane == null)
            this.createView();
        return anchorPane;
    }

    /**
     * Event handler for the 'Connect' button.
     * <p>
     * This object is responsible for grabbing all the information the user put
     * it and create a UserConfig object from it.
     * <p>
     * It will also check if a connection can be made to the server, otherwise
     * it will display an alert informing the user that a connection couldn't be
     * made.
     * <p>
     * If a connection could be made, it will load the next view.
     * 
     */
    private EventHandler<ActionEvent> onBtnEvent = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {

            if (event.getSource() == startBtn) {
                UserConfig userCfg = new UserConfig();
                userCfg.setUsername(userNameInput.getText());
                userCfg.setHost(ipInput.getText());
                userCfg.setPort(portInput.getText());

                mainApp.setUserCfg(userCfg);

                try {
                    mainApp.startChatClient();
                } catch (IOException e) {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Could not connect");
                    alert.setHeaderText("Could not connect to the server.");
                    alert.setContentText("Make sure you're using the correct IP and Port, or that\n"
                            + "the server you're trying to connect to is running.");

                    alert.showAndWait();
                    return;
                }

                mainApp.setView(mainApp.getChatView());
            }

        }
    };

    /**
     * Creates the view.
     */
    private void createView() {
        anchorPane = new AnchorPane();
        vbox = new VBox(10);

        Label label = new Label("Username");

        userNameInput = new TextField();
        userNameInput.setText("User-" + new Random().nextInt(9000));
        userNameInput.setAlignment(Pos.CENTER);

        vbox.getChildren().add(label);
        vbox.getChildren().add(userNameInput);

        label = new Label("Host");

        ipInput = new TextField();
        ipInput.setText("127.1.0.0");
        ipInput.setAlignment(Pos.CENTER);

        vbox.getChildren().add(label);
        vbox.getChildren().add(ipInput);

        label = new Label("Port");

        portInput = new TextField();
        portInput.setText("4242");
        portInput.setAlignment(Pos.CENTER);

        vbox.getChildren().add(label);
        vbox.getChildren().add(portInput);

        startBtn = new Button("Connect");
        startBtn.setPrefWidth(160.0);
        startBtn.setOnAction(onBtnEvent);

        vbox.getChildren().add(startBtn);

        AnchorPane.setTopAnchor(vbox, 45.0);
        AnchorPane.setLeftAnchor(vbox, 130.0);
        AnchorPane.setRightAnchor(vbox, 130.0);

        anchorPane.getChildren().add(vbox);
    }

}
