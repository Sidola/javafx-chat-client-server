package application.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.SocketException;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import application.view.ConfigView;

/**
 * ClientThread implementation for the Client-side of the application.
 * <p>
 * This class is responsible for receiving input from the server and displaying
 * it in the ChatView.
 * 
 * It will also notify the user if the connection to the server is lost and
 * change back to the ConfigView.
 * 
 * @author Sid
 */
public class ClientThread implements Runnable {

    private ChatClient chatClient;
    private BufferedWriter outStream;
    private BufferedReader inStream;

    /**
     * Constructor.
     * 
     * @param chatClient
     *            Reference back to the ChatClient that owns this thread.
     * @param outStream
     *            Reference to the OutputStream.
     * @param inStream
     *            Reference to the InputStream.
     */
    public ClientThread(ChatClient chatClient, BufferedWriter outStream,
            BufferedReader inStream) {
        this.chatClient = chatClient;
        this.outStream = outStream;
        this.inStream = inStream;
    }

    /**
     * This method will put itself in an endless loop and listen for input from
     * the server.
     * <p>
     * When it receives input, it will append it to the TextArea in the ChatView
     * class.
     */
    @Override
    public void run() {
        try {
            while (true) {
                String message = inStream.readLine();
                chatClient.getMainApp().appendToChat(message + "\n");
            }
        } catch (SocketException e) {
            // If we end up in here, the client has lost the connection
            // And we need to close everything gracefully

            // We need to call the JavaFX thread like this since we can only
            // perform operations on that thread from within the thread itself.
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Connection Lost");
                    alert.setHeaderText("Lost the connection to the server.");
                    alert.setContentText("The connection to the server was lost.");
                    alert.showAndWait();

                    // Return to the config view
                    chatClient.getMainApp().setView(new ConfigView());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a reference to the BufferedWriter that belongs to this thread.
     * 
     * @return a reference to the BufferedWriter associated with this thread
     */
    public BufferedWriter getWriter() {
        return this.outStream;
    }

}
