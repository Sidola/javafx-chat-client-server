package application.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import application.ChatApplication;

/**
 * This class is responsible for creating a connection to the chat server and
 * exchanging messages with it.
 * 
 * @author Sid
 */
public class ChatClient {

    private ChatApplication mainApp;

    private Socket clientSocket;
    private ClientThread clientThread;

    /**
     * Constructor. Accepts a reference back to the main application.
     * 
     * @param mainApp
     *            Reference to the main application.
     */
    public ChatClient(ChatApplication mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Connects to the server and creates a new thread that will listen for
     * messages from the server and display them in the chat.
     * 
     * @throws IOException
     *             Throws an IOException if the server could not be reached for
     *             whatever reason.
     */
    public void start() throws IOException {
        String host = mainApp.getUserCfg().getHost();
        int port = Integer.valueOf(mainApp.getUserCfg().getPort());

        clientSocket = new Socket(host, port);

        // Get streams
        BufferedWriter outStream = new BufferedWriter(new OutputStreamWriter(
                clientSocket.getOutputStream(), "UTF-8"));
        outStream.flush(); // Flush directly after creating
        BufferedReader inStream = new BufferedReader(new InputStreamReader(
                clientSocket.getInputStream(), "UTF-8"));

        // Create a new thread
        clientThread = new ClientThread(this, outStream, inStream);

        Thread thread = new Thread(clientThread);
        thread.start();
    }

    /**
     * Sends a message to the server.
     * 
     * @param message
     *            Message to send.
     */
    public void sendMessage(String message) {
        try {
            clientThread.getWriter().write(message);
            clientThread.getWriter().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a reference to the main application.
     * 
     * @return reference to the main application
     */
    protected ChatApplication getMainApp() {
        return mainApp;
    }

}
